#!/bin/zsh
debug="-v"
alias cd="cd"
alias cp="cp -R $debug"
alias find="find"
alias mv="mv $debug"
alias svn="svn"
alias rm="rm $debug"
alias rsync="rsync --recursive --archive --copy-links $debug"
alias ziptest="unzip -tq"
alias zip="zip --recurse-paths $debug"
SVN="SVN"
slug="systasis-gf-infusionsoft-feed"
gitRoot="/Volumes/APFS/git"
workRoot="$gitRoot/gravity-forms-infusionsoft-add-on"
svndir="$gitRoot/$slug"
tree=`git symbolic-ref --short HEAD`

here=`pwd`

#
# get build info
#
rm -rf /tmp/.git-commit-id
COMPILER=`php -v | head -1`
VERSION=`git log $tree --decorate | head -1`
printf "%s\n%s\n" $COMPILER $VERSION > /tmp/.git-commit-id

#
# get current SVN
#
if [ "X"$1 = "Xsvn"  ]
  then
    rm -rf $svndir
    svn co https://plugins.svn.wordpress.org/systasis-gf-infusionsoft-feed $svndir
fi

#
# Composer
#
if [ "X"$1 = "Xcomposer" ]
  then
    cd $workRoot/$slug
    dest="./vendor/systasis/infusionsoft"
    rm -rf $dest
    git clone $gitRoot/infusionsoft-php-sdk $dest
    rm -rf $dest/.git
    composer dump-autoload
fi

#
# Refresh trunk
#
if [ "X"$1 = "Xrefresh" ]
  then
    rsync ../assets $svndir

    rsync --exclude='.git' $gitRoot/infusionsoft-php-sdk $svndir/trunk/vendor/systasis
    mv $svndir/trunk/vendor/systasis/infusionsoft-php-sdk $svndir/trunk/vendor/systasis/infusionsoft

    rsync -v $slug/ $svndir/trunk/ \
    --exclude '.*' \
    --exclude 'composer.json' \
    --exclude 'composer.lock' \
    --exclude '*nstalled*' \
    --exclude 'README.md' \
    --exclude 'forceutf8/resources' \
    --exclude 'forceutf8/test' \
    --exclude 'forceutf8/composer.json'
# --exclude '*assets*' \
# --exclude '*sh' \
# --exclude '*zip' \
# --exclude '*Test*' \
# --exclude '*SVN*' \
# --exclude '*/example*' \
# --exclude 'vendor/bin/' \
# --exclude '*/utilities*' \
# --exclude '*infusionsoft-php-sdk/composer*' \

    echo "svn status $svndir | grep --colour=never \"^[ ?]\""
    svn status $svndir | grep --colour=never "^[ ?]"
fi

#
# Create zip
#
if [ "X"$1 = "Xzip" ]
  then
    # git archive $debug --output=$gitRoot/$slug.zip $tree $slug # --add-file=/tmp/.git-commit-id
    cd $svndir/trunk
    rm $gitRoot/$slug.zip
    zip $gitRoot/$slug.zip .
    ziptest $gitRoot/$slug.zip
fi

cd $here